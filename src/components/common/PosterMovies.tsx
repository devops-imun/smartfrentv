import React from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Palette } from '../../utils/palette';
import { width } from '../../utils/size';

const PosterMovies = ({ item, navigation, isMovie = true }: any) => {
  return (
    <TouchableOpacity
      onPress={() =>
        isMovie
          ? navigation.navigate('DetailMovie', {
              data: item,
            })
          : navigation.navigate('DetailTV', {
              data: item,
            })
      }
      style={styles.card}
    >
      <Image
        source={{
          uri: item?.poster?.medium,
        }}
        style={styles.image}
      />
    </TouchableOpacity>
  );
};

export default PosterMovies;

const styles = StyleSheet.create({
  card: {
    width: width / 3.5,
    height: width / 2.5,
    borderRadius: 10,
    overflow: 'hidden',
    marginLeft: 15,
    shadowColor: Palette.shadow,
    shadowOffset: {
      width: 2,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 5,
  },
  image: {
    width: width / 3.5,
    height: width / 2.5,
  },
});
