import React from 'react';
import { Text, View } from 'react-native';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import { TouchableRipple } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ContentTitle = ({ title, onPress }: any) => {
  return (
    <TouchableRipple onPress={onPress} rippleColor={Palette.text}>
      <View style={styles.button}>
        <Text style={styles.title}>{title}</Text>
        <Ionicons name={'arrow-forward'} size={20} color={Palette.text} />
      </View>
    </TouchableRipple>
  );
};

export default ContentTitle;

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    ...textStyles.xLargeBold,
    color: Palette.primary,
  },
});
