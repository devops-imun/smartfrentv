import React from 'react';
import { StyleSheet, View } from 'react-native';
import { width } from '../../utils/size';
import { Palette } from '../../utils/palette';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const PosterShimmer = () => {
  return (
    <SkeletonPlaceholder
      speed={1200}
      backgroundColor={Palette.shadow}
      highlightColor={Palette.border}
    >
      <View style={styles.card} />
    </SkeletonPlaceholder>
  );
};

export default PosterShimmer;

const styles = StyleSheet.create({
  card: {
    width: width / 3.5,
    height: width / 2.5,
    borderRadius: 10,
    overflow: 'hidden',
    marginLeft: 15,
    shadowColor: Palette.shadow,
    shadowOffset: {
      width: 2,
      height: 0,
    },
    shadowOpacity: 0.23,
    shadowRadius: 5,
  },
});
