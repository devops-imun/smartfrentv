import React from 'react';
import { FlatList } from 'react-native';
import PosterShimmer from './PosterShimmer';

let data: any = [];

for (let index = 0; index < 5; index++) {
  data.push(index);
}

const LoaderListHorizontal = () => {
  return (
    <FlatList
      horizontal
      data={data}
      renderItem={({}: any) => <PosterShimmer />}
      keyExtractor={(item, index) => 'key' + index}
    />
  );
};

export default LoaderListHorizontal;
