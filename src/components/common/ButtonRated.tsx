/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';

const ButtonRated = ({
  onPress,
  icon = 'thumb-up-outline',
  title = 'title',
}: any) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        ...styles.button,
        marginLeft: title === 'Dislike' ? 17 : 0,
      }}
    >
      <MaterialCommunityIcons name={icon} size={25} color={Palette.primary} />
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonRated;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  title: {
    ...textStyles.largeBold,
    color: Palette.primary,
  },
});
