/*
  interface movies
*/

interface Poster {
  medium: string;
}

interface Backdrop {
  medium: string;
}

interface MoviesReview {
  id: string;
  author: string;
  content: string;
}

interface MovieGenres {
  name: string;
}

interface MoviesData {
  id: string;
  name: string;
  overview: string;
  tagline: string;
  runtime: number;
  genres: MovieGenres[];
  releaseDate: Date;
  poster: Poster;
  backdrop: Backdrop;
  review: MoviesReview;
}

interface MoviesType {
  movies: MoviesData[];
}

/*
  interface TV
*/
interface TvSeasons {
  id: string;
  name: string;
}

interface TvReviews {
  id: string;
  author: string;
  content: string;
}

interface TvData {
  id: string;
  name: string;
  overview: string;
  popularity: string;
  status: string;
  score: string;
  runtime: string;
  seasons: TvSeasons;
  reviews: TvReviews;
  poster: Poster;
  backdrop: Backdrop;
}

interface TvType {
  tv: TvData[];
}

/*
  interface people
*/

interface PeopleImage {
  medium: string;
}
interface PeopleData {
  id: string;
  name: string;
  biography: string;
  gender: string;
  birthday: string;
  aliases: string;
  birthplace: string;
  images: PeopleImage[];
}

interface PeopleType {
  people: PeopleData[];
}

/*
  interface search movie
*/

interface SearchData {
  id: string;
  name: string;
  overview: string;
  score: number;
  runtime: number;
  backdrop: Backdrop;
  poster: Poster;
}
interface SearchType {
  movies: SearchData[];
}
/*
  interface navigation
*/

interface RouteType {
  params: any;
}

interface NavigationType {
  route?: RouteType;
  navigation: any;
}
