import { gql, useQuery } from '@apollo/client';

export const GET_AIRING_TODAY = gql`
  query airingToday {
    tv: airingToday {
      id
      name
      overview
      popularity
      status
      score
      runtime
      seasons {
        id
        name
      }
      reviews {
        id
        author
        content
      }
      poster {
        medium
      }
      backdrop {
        medium
      }
    }
  }
`;

export default <TvType>() => {
  return useQuery<TvType>(GET_AIRING_TODAY);
};
