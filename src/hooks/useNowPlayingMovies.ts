import { gql, useQuery } from '@apollo/client';

export const GET_NOW_PLAYING_MOVIE = gql`
  query nowPlayingMovie {
    movies: nowPlaying {
      id
      name
      overview
      releaseDate
      score
      tagline
      runtime
      genres {
        name
      }
      backdrop {
        medium
      }
      poster {
        medium
      }
      reviews {
        id
        author
        content
      }
    }
  }
`;

export default <NowPlayingMovies>() => {
  return useQuery<NowPlayingMovies>(GET_NOW_PLAYING_MOVIE);
};
