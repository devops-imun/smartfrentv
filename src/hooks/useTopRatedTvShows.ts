import { gql, useQuery } from '@apollo/client';

export const GET_TOP_RATED_TV_SHOWS = gql`
  query topRatedTvShow {
    tv: topRatedTV {
      id
      name
      overview
      popularity
      status
      score
      runtime
      seasons {
        id
        name
      }
      reviews {
        id
        author
        content
      }
      poster {
        medium
      }
      backdrop {
        medium
      }
    }
  }
`;

export default <TvType>() => {
  return useQuery<TvType>(GET_TOP_RATED_TV_SHOWS);
};
