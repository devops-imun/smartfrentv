import { gql, useQuery } from '@apollo/client';

export const GET_POPULAR_MOVIE = gql`
  query popularMovies {
    movies: popularMovies {
      id
      name
      overview
      releaseDate
      score
      tagline
      runtime
      genres {
        name
      }
      backdrop {
        medium
      }
      poster {
        medium
      }
      reviews {
        id
        author
        content
      }
    }
  }
`;

export default <TopRatedMovies>() => {
  return useQuery<TopRatedMovies>(GET_POPULAR_MOVIE);
};
