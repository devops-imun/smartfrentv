import { gql, useQuery } from '@apollo/client';

export const GET_UPCOMING_MOVIES = gql`
  query upcomingMovies {
    movies: upcomingMovies {
      id
      name
      overview
      releaseDate
      score
      tagline
      runtime
      genres {
        name
      }
      backdrop {
        medium
      }
      poster {
        medium
      }
      reviews {
        id
        author
        content
      }
    }
  }
`;

export default <TopRatedMovies>() => {
  return useQuery<TopRatedMovies>(GET_UPCOMING_MOVIES);
};
