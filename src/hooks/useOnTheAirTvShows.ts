import { gql, useQuery } from '@apollo/client';

export const GET_ON_THE_AIR_TV_SHOWS = gql`
  query onTheAirTv {
    tv: discoverTV {
      id
      name
      overview
      popularity
      status
      score
      runtime
      seasons {
        id
        name
      }
      reviews {
        id
        author
        content
      }
      poster {
        medium
      }
      backdrop {
        medium
      }
    }
  }
`;

export default <TvType>() => {
  return useQuery<TvType>(GET_ON_THE_AIR_TV_SHOWS);
};
