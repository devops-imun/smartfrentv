import { gql, useQuery } from '@apollo/client';

export const GET_POPULAR_PEOPLE = gql`
  query popularPeople {
    people: popularPeople {
      id
      name
      biography
      gender
      birthday
      aliases
      birthplace
      images {
        medium
      }
    }
  }
`;

export default <PeopleType>() => {
  return useQuery<PeopleType>(GET_POPULAR_PEOPLE);
};
