import { gql, useLazyQuery } from '@apollo/client';

const SEARCH_MOVIES = gql`
  query SearchMovies($search: String!) {
    movies: searchMovies(query: $search) {
      id
      name
      overview
      score
      runtime
      poster {
        medium
      }
      backdrop {
        medium
      }
    }
  }
`;

export default <SearchType>() => {
  return useLazyQuery<SearchType>(SEARCH_MOVIES, {
    fetchPolicy: 'network-only',
  });
};
