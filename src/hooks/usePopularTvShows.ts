import { gql, useQuery } from '@apollo/client';

export const GET_POPULAR_TV_SHOWS = gql`
  query popularTvShow {
    tv: popularTV {
      id
      name
      overview
      popularity
      status
      score
      runtime
      seasons {
        id
        name
      }
      reviews {
        id
        author
        content
      }
      poster {
        medium
      }
      backdrop {
        medium
      }
    }
  }
`;

export default <TvType>() => {
  return useQuery<TvType>(GET_POPULAR_TV_SHOWS);
};
