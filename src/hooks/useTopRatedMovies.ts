import { gql, useQuery } from '@apollo/client';

export const GET_TOP_RATED_MOVIE = gql`
  query topRatedMovies {
    movies: topRatedMovies {
      id
      name
      overview
      releaseDate
      score
      tagline
      runtime
      genres {
        name
      }
      backdrop {
        medium
      }
      poster {
        medium
      }
      reviews {
        id
        author
        content
      }
    }
  }
`;

export default <TopRatedMovies>() => {
  return useQuery<TopRatedMovies>(GET_TOP_RATED_MOVIE);
};
