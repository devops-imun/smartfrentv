import React, { useEffect } from 'react';
import { ActivityIndicator, StyleSheet, FlatList, View } from 'react-native';
import usePopularPeople from '../../hooks/usePopularPeople';
import { Palette } from '../../utils/palette';
import { width } from '../../utils/size';
import textStyles from '../../utils/typography';
import PopularPeopleItem from './components/PopularPeopleItem';

const PopularPeople = ({ navigation }: any) => {
  const { loading, data } = usePopularPeople<PeopleType>();
  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Popular People',
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [navigation]);

  useEffect(() => {
    console.log(data);
  }, [data?.people, data]);
  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.loader}>
          <ActivityIndicator size={25} color={Palette.text} />
        </View>
      ) : (
        <FlatList<PeopleData>
          data={data?.people}
          renderItem={({ item }) => (
            <PopularPeopleItem key={item.id} item={item} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </View>
  );
};

export default PopularPeople;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    width: width,
    height: width,
    overflow: 'hidden',
  },
  image: {
    width: width,
    height: width / 1.5,
  },
});
