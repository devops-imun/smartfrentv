import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Palette } from '../../../utils/palette';
import { width } from '../../../utils/size';
import textStyles from '../../../utils/typography';
import moment from 'moment';

const PopularPeopleItem = ({ item }: any) => {
  return (
    <View style={styles.card}>
      <Image style={styles.image} source={{ uri: item?.images[0]?.medium }} />
      <View style={styles.spacer} />
      <View style={styles.content}>
        <Text style={styles.name}>{item?.name}</Text>
        <Text style={styles.gender}>{item?.gender}</Text>
        <Text style={styles.birth}>
          {moment(item?.birthday).format('dddd, DD MMMM YYYY')}
        </Text>
        <Text numberOfLines={2} style={styles.place}>
          {item?.birthplace}
        </Text>
        <Text numberOfLines={7} style={styles.biography}>
          {item?.biography}
        </Text>
      </View>
    </View>
  );
};

export default PopularPeopleItem;

const styles = StyleSheet.create({
  card: {
    width: width,
    flexDirection: 'row',
    marginBottom: 20,
  },
  content: {
    width: width - width / 2.4,
  },
  spacer: {
    margin: 10,
  },
  image: {
    width: width / 3,
    height: width / 2,
  },
  name: {
    ...textStyles.xLargeBold,
    color: Palette.primary,
  },
  gender: {
    ...textStyles.largeMedium,
    color: 'red',
  },
  birth: {
    ...textStyles.largeMedium,
    color: 'red',
  },
  place: {
    ...textStyles.largeMedium,
    color: 'red',
  },
  biography: {
    ...textStyles.largeLight,
    color: Palette.shadow,
  },
});
