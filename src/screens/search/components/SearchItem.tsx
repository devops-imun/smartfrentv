import React from 'react';
import { Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import textStyles from '../../../utils/typography';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Palette } from '../../../utils/palette';
import { width } from '../../../utils/size';

const SearchItem = ({ item, navigation }: any) => {
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('DetailMovie', {
          data: item,
        })
      }
      style={styles.card}
    >
      <View style={styles.row}>
        <Image source={{ uri: item?.poster?.medium }} style={styles.poster} />
        <View style={styles.spacerSmall} />
        <View style={styles.content}>
          <Text numberOfLines={2} style={styles.subtitle}>
            {item?.name}
          </Text>
          <Text style={styles.subtitle}>{item?.runtime} mins</Text>
          <View style={styles.row}>
            <Ionicons name={'star'} size={18} color={'gold'} />
            <Text style={styles.subtitle}>{item?.score}</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity>
        <MaterialCommunityIcons
          name={'playlist-check'}
          size={30}
          color={Palette.primary}
        />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default SearchItem;

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    marginVertical: 5,
    justifyContent: 'space-between',
  },
  content: {
    width: width / 2,
  },
  spacer: {
    margin: 10,
  },
  spacerSmall: {
    margin: 5,
  },
  row: {
    flexDirection: 'row',
  },
  poster: {
    width: 80,
    height: 100,
    borderRadius: 5,
  },
  subtitle: {
    ...textStyles.largeMedium,
    color: 'grey',
  },
});
