import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Text,
  FlatList,
  StyleSheet,
  View,
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import useSearchMovie from '../../hooks/useSearchMovie';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import SearchItem from './components/SearchItem';

const SearchScreen = ({ navigation }: any) => {
  const [search, setSearch] = useState<string>('');
  const [searchMovie, { loading, data, error }] = useSearchMovie<SearchType>();

  useEffect(() => {
    searchMovie({
      variables: {
        search: search,
      },
    });
  }, [search, searchMovie]);

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [navigation]);

  console.log(`LOADING :${error}`);
  console.log(`SEARCH :${data?.movies}`);

  return (
    <View style={styles.container}>
      <SearchBar
        placeholder="Search movie..."
        onChangeText={setSearch}
        value={search}
        inputStyle={styles.inputStyle}
        containerStyle={styles.containerStyle}
        inputContainerStyle={styles.inputContainerStyle}
        placeholderTextColor={'white'}
      />
      {loading ? (
        <View style={styles.loader}>
          <ActivityIndicator size={25} color={Palette.text} />
        </View>
      ) : (
        <FlatList<SearchData>
          data={data?.movies}
          renderItem={({ item }) => (
            <SearchItem key={item?.id} item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
          ListEmptyComponent={() => (
            <View style={styles.emptyContainer}>
              <Text style={styles.title}>Movie List Empty</Text>
            </View>
          )}
        />
      )}
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputStyle: { backgroundColor: Palette.border },
  containerStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderRadius: 5,
  },
  inputContainerStyle: { backgroundColor: Palette.border, borderRadius: 10 },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    ...textStyles.largeSemibold,
    color: Palette.primary,
  },
});
