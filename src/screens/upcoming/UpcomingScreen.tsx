import React, { useEffect } from 'react';
import { ActivityIndicator, StyleSheet, FlatList, View } from 'react-native';
import useUpcomingMovies from '../../hooks/useUpcomingMovies';
import { Palette } from '../../utils/palette';
import { width } from '../../utils/size';
import textStyles from '../../utils/typography';
import UpcomingItem from './components/UpcomingItem';

const UpcomingScreen = ({ navigation }: any) => {
  const { loading, data } = useUpcomingMovies<MoviesType>();
  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Upcoming Movies',
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [navigation]);

  useEffect(() => {
    console.log('');
  }, [data?.movies, data]);
  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.loader}>
          <ActivityIndicator size={25} color={Palette.text} />
        </View>
      ) : (
        <FlatList<MoviesData>
          data={data?.movies}
          renderItem={({ item }) => (
            <UpcomingItem key={item.id} item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </View>
  );
};

export default UpcomingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    width: width,
    height: width,
    overflow: 'hidden',
  },
  image: {
    width: width,
    height: width / 1.5,
  },
});
