import React, { useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

const WatchListScreen = ({ navigation }: any) => {
  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Smartfren TV',
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
        fontSize: 25,
        fontStyle: 'italic',
      },
      headerLeft: () => (
        <TouchableOpacity
          style={styles.actionMenu}
          onPress={() => navigation.openDrawer()}
        >
          <Ionicons name={'menu'} color={Palette.primary} size={25} />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity style={styles.actionMenu}>
          <Ionicons name={'notifications'} color={Palette.primary} size={25} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text>Watch List</Text>
      </View>
    </SafeAreaView>
  );
};

export default WatchListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionMenu: {
    padding: 15,
  },
});
