import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useTopRatedMovies from '../../../hooks/useTopRatedMovies';
import { Palette } from '../../../utils/palette';

const TopRatedMovie = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useTopRatedMovies<MoviesType>();

  useEffect(() => {
    console.log(error);
  }, [data?.movies, error]);
  return (
    <>
      <ContentTitle
        title={'Top Rated'}
        onPress={() => {
          navigation.navigate('ListCategoryMovies', {
            title: 'Top Rated',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<MoviesData>
          horizontal
          data={data?.movies}
          renderItem={({ item }) => (
            <PosterMovies item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default TopRatedMovie;
