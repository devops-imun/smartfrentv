import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useNowPlayingMovies from '../../../hooks/useNowPlayingMovies';
import { Palette } from '../../../utils/palette';

const NowPlayingMovie = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useNowPlayingMovies<MoviesType>();

  useEffect(() => {
    console.log(error);
  }, [data?.movies, error]);
  return (
    <>
      <ContentTitle
        title={'Now Playing'}
        onPress={() => {
          navigation.navigate('ListCategoryMovies', {
            title: 'Now Playing',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<MoviesData>
          horizontal
          data={data?.movies}
          renderItem={({ item }) => (
            <PosterMovies item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default NowPlayingMovie;
