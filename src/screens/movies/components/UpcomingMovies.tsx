import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useUpcomingMovies from '../../../hooks/useUpcomingMovies';
import { Palette } from '../../../utils/palette';

const UpComingMovie = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useUpcomingMovies<MoviesType>();

  useEffect(() => {
    console.log(error);
  }, [data?.movies, error]);
  return (
    <>
      <ContentTitle
        title={'Upcoming'}
        onPress={() => {
          navigation.navigate('ListCategoryMovies', {
            title: 'Upcoming',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<MoviesData>
          horizontal
          data={data?.movies}
          renderItem={({ item }) => (
            <PosterMovies item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default UpComingMovie;
