import React, { useEffect } from 'react';
import { ActivityIndicator, StyleSheet, FlatList, View } from 'react-native';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import UpcomingItem from '../upcoming/components/UpcomingItem';

const ListCategoryMovies = ({ route, navigation }: NavigationType) => {
  const { loading, data, title } = route?.params;
  useEffect(() => {
    navigation.setOptions({
      headerTitle: title,
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [data.name, navigation, route, title]);
  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<MoviesData>
          data={data?.movies}
          renderItem={({ item }) => (
            <UpcomingItem item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </View>
  );
};

export default ListCategoryMovies;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
