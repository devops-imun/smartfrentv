import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import { Palette } from '../../utils/palette';
import { width } from '../../utils/size';
import TopRatedMovie from './components/TopRatedMovie';
import NowPlayingMovie from './components/NowPlayingMovie';
import UpComingMovie from './components/UpcomingMovies';
import PopularMovie from './components/PopularMovie';

const MoviesScreen = ({ navigation }: NavigationType) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <TopRatedMovie navigation={navigation} />
          <View style={styles.spacer} />
          <NowPlayingMovie navigation={navigation} />
          <View style={styles.spacer} />
          <UpComingMovie navigation={navigation} />
          <View style={styles.spacer} />
          <PopularMovie navigation={navigation} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default MoviesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionMenu: {
    padding: 15,
  },
  spacer: {
    marginTop: 15,
  },
  cardCarousel: {
    borderRadius: 10,
    elevation: 5,
    marginHorizontal: 15,
    marginVertical: 15,
    width: width - 30,
    height: width / 1.5,
    backgroundColor: Palette.card,
    shadowColor: Palette.primary,
    shadowOffset: {
      width: 2,
      height: 0,
    },
    overflow: 'hidden',
    shadowOpacity: 0.23,
    shadowRadius: 5,
  },
});
