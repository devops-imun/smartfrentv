import React, { useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {
  // getFocusedRouteNameFromRoute,
  NavigationContainer,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SearchScreen from '../search/SearchScreen';
import SettingScreen from '../setting/SettingScreen';
import { navigationRef } from '../../utils/navigation_ref';
import PeopleScreen from '../people/PeopleScreen';
import { Palette } from '../../utils/palette';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MoviesScreen from '../movies/MoviesScreen';
import TvShowScreen from '../tv/TvShowScreen';
import UpcomingScreen from '../upcoming/UpcomingScreen';
import MyDrawerContent from './DrawerContent';
import textStyles from '../../utils/typography';
import { TouchableOpacity } from 'react-native';
import DetailMovie from '../movies/DetailMovie';
import ListCategoryMovies from '../movies/ListCategoryMovies';
import ListCategoryTV from '../tv/ListCategoryTV';
import DetailTV from '../tv/DetailTV';
import WatchListScreen from '../watchlist/WatchListScreen';
import SplashScreen from 'react-native-splash-screen';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const TopTabs = createMaterialTopTabNavigator();

// let routeName: any;

const MyTheme = {
  dark: false,
  colors: {
    primary: Palette.primary,
    background: 'black',
    card: 'black',
    text: Palette.text,
    border: Palette.border,
    notification: Palette.primary,
  },
};

const TopTabsHomeScreen = () => {
  return (
    <TopTabs.Navigator
      tabBarOptions={{
        labelStyle: { textTransform: 'none' },
        scrollEnabled: false,
      }}
    >
      <TopTabs.Screen
        name={'HomeTvShow'}
        component={StackTvShowsScreen}
        options={{
          tabBarLabel: 'Tv Show',
        }}
      />
      <TopTabs.Screen
        name={'HomeMovies'}
        component={StackMoviesScreen}
        options={{
          tabBarLabel: 'Movies',
        }}
      />
      <TopTabs.Screen
        name={'HomeWatchList'}
        component={StackWatchListScreen}
        options={{
          tabBarLabel: 'Watchlist',
        }}
      />
    </TopTabs.Navigator>
  );
};

const StackHomeScreen = ({ navigation }: any) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'MainHomeScreen'}
        component={TopTabsHomeScreen}
        options={{
          title: 'Smartfren TV',
          headerTitleStyle: {
            ...textStyles.xLargeSemibold,
          },
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={{ marginLeft: 15 }}
            >
              <Ionicons name={'menu'} size={30} color={Palette.text} />
            </TouchableOpacity>
          ),
        }}
      />
      <Stack.Screen
        name={'ListCategoryMovies'}
        component={ListCategoryMovies}
      />
      <Stack.Screen name={'ListCategoryTV'} component={ListCategoryTV} />
      <Stack.Screen name={'DetailMovie'} component={DetailMovie} />
      <Stack.Screen name={'DetailTV'} component={DetailTV} />
    </Stack.Navigator>
  );
};

const StackTvShowsScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'MainTvShowScreen'}
        component={TvShowScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const StackMoviesScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'MainMoviesScreen'}
        component={MoviesScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const StackWatchListScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'MainWatchListScreen'}
        component={WatchListScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const StackUpcomingScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={'MainUpcomingScreen'} component={UpcomingScreen} />
    </Stack.Navigator>
  );
};

const StackSearchScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={'MainSearchScreen'} component={SearchScreen} />
    </Stack.Navigator>
  );
};

const StackPeopleScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={'MainPeopleScreen'} component={PeopleScreen} />
    </Stack.Navigator>
  );
};

const TabsMainNavigator = () => {
  return (
    <Tabs.Navigator
      initialRouteName="HomeTab"
      lazy={true}
      tabBarOptions={{
        activeTintColor: Palette.primary,
        inactiveTintColor: 'grey',
        tabStyle: {
          paddingTop: 5,
        },
      }}
    >
      <Tabs.Screen
        name={'HomeTab'}
        component={StackHomeScreen}
        options={{
          title: 'Home',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name={'home'} size={size} color={color} />;
          },
        }}
      />
      <Tabs.Screen
        name={'SearchTab'}
        component={StackSearchScreen}
        options={{
          title: 'Search',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name={'search'} size={size} color={color} />;
          },
        }}
      />
      <Tabs.Screen
        name={'UpcomingTab'}
        component={StackUpcomingScreen}
        options={{
          title: 'Upcoming',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name={'tv'} size={size} color={color} />;
          },
        }}
      />
      <Tabs.Screen
        name={'PopularTab'}
        component={StackPeopleScreen}
        options={{
          title: 'People',
          tabBarIcon: ({ color, size }) => {
            return (
              <Ionicons name={'md-people-sharp'} size={size} color={color} />
            );
          },
        }}
      />
    </Tabs.Navigator>
  );
};

const DrawerMainNavigator = () => {
  return (
    <Drawer.Navigator
      lazy={true}
      initialRouteName="MainDrawer"
      drawerType="slide"
      overlayColor="transparent"
      drawerContent={() => {
        return <MyDrawerContent />;
      }}
    >
      <Drawer.Screen name={'MainDrawer'} component={TabsMainNavigator} />
      <Drawer.Screen name={'SettingScreen'} component={SettingScreen} />
    </Drawer.Navigator>
  );
};

const RouteNavigator = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <NavigationContainer theme={MyTheme} ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name={'Main'} component={DrawerMainNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RouteNavigator;
