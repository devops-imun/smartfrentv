import { DrawerContentScrollView } from '@react-navigation/drawer';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

const MyDrawerContent = () => {
  return (
    <DrawerContentScrollView>
      <View style={styles.container}>
        <View style={styles.drawerHeader}>
          <Text style={styles.headerTitle}>Movies & TV</Text>
        </View>
        <View style={styles.drawerContent}>
          <TouchableOpacity style={styles.menuDrawer}>
            <Ionicons name={'settings-sharp'} size={20} color={Palette.text} />
            <Text style={styles.titleMenuDrawer}>Setting</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuDrawer}>
            <Ionicons
              name={'information-circle'}
              size={20}
              color={Palette.text}
            />
            <Text style={styles.titleMenuDrawer}>Setting</Text>
          </TouchableOpacity>
        </View>
      </View>
    </DrawerContentScrollView>
  );
};

export default MyDrawerContent;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    borderLeftWidth: 0.5,
    borderLeftColor: 'transparent',
  },
  drawerHeader: {
    paddingHorizontal: 10,
    paddingVertical: 30,
    backgroundColor: Palette.border,
  },
  headerTitle: {
    ...textStyles.xLargeSemibold,
    color: Palette.text,
  },
  drawerContent: {
    marginTop: 10,
  },
  menuDrawer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  titleMenuDrawer: {
    color: Palette.text,
    marginLeft: 15,
    ...textStyles.largeBold,
  },
});
