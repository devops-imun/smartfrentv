import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';

const SettingScreen = () => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text>Setting</Text>
      </View>
    </SafeAreaView>
  );
};

export default SettingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
