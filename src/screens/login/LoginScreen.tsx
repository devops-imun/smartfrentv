import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';

const LoginScreen = () => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text>Login</Text>
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
