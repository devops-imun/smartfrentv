import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import AiringTV from './components/AiringTV';
import OnTheAirTV from './components/OnTheAirTV';
import PopularTV from './components/PopularTV';
import TopRatedTv from './components/TopRatedTV';

const TvShowScreen = ({ navigation }: any) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <TopRatedTv navigation={navigation} />
          <View style={styles.spacer} />
          <PopularTV navigation={navigation} />
          <View style={styles.spacer} />
          <OnTheAirTV navigation={navigation} />
          <View style={styles.spacer} />
          <AiringTV navigation={navigation} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TvShowScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  spacer: {
    marginTop: 15,
  },
});
