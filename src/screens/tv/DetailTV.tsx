import React, { useEffect } from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Palette } from '../../utils/palette';
import { width } from '../../utils/size';
import textStyles from '../../utils/typography';
import moment from 'moment';
import ButtonRated from '../../components/common/ButtonRated';
import LinearGradient from 'react-native-linear-gradient';

const DetailTV = ({ route, navigation }: NavigationType) => {
  const { data } = route?.params;

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
      headerTitle: data?.name,
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [data?.name, navigation, route]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={{ uri: data?.backdrop?.medium }}
        style={styles.backdrop}
      >
        <LinearGradient
          colors={['transparent', 'black']}
          style={styles.backdrop}
        >
          <View style={styles.appbar}>
            <TouchableOpacity onPress={() => navigation.pop()}>
              <Ionicons name={'arrow-back'} size={25} color={Palette.primary} />
            </TouchableOpacity>
          </View>
          <View style={styles.headerContent}>
            <Image
              source={{ uri: data?.poster?.medium }}
              style={styles.poster}
            />
            <View style={styles.spacerSmall} />
            <View>
              <Text style={styles.title}>{data?.name}</Text>
              <Text style={styles.subtitle}>{data?.runtime} mins</Text>
              <View style={styles.row}>
                <Text style={styles.subtitle}>{data?.score}</Text>
                <Ionicons name={'star'} size={20} color={'gold'} />
              </View>
              <Text style={styles.subtitle}>
                {moment(data?.releaseDate).format('DD MMM YYYY')}
              </Text>
            </View>
          </View>
        </LinearGradient>
      </ImageBackground>
      <View style={styles.contentMain}>
        <View style={styles.rateBar}>
          <ButtonRated
            icon={'thumb-up-outline'}
            title="Like"
            onPress={() => {
              console.log('Like');
            }}
          />
          <ButtonRated
            icon={'thumb-down-outline'}
            title="Dislike"
            onPress={() => {
              console.log('');
            }}
          />
          <ButtonRated
            icon={'playlist-plus'}
            title="Watchlist"
            onPress={() => {
              console.log('Watchlist');
            }}
          />
        </View>
        <View style={styles.spacer} />
        <Text style={styles.overview}>{data?.overview}</Text>
      </View>
    </View>
  );
};

export default DetailTV;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  appbar: {
    position: 'absolute',
    top: 15,
    left: 15,
  },
  backdrop: {
    width: width,
    height: width / 1.5,
  },
  poster: {
    width: width / 3.5,
    height: width / 2.5,

    borderRadius: 10,
  },
  headerContent: {
    position: 'absolute',
    bottom: -(width / 2.5) / 2 + 20,
    left: 25,
    flexDirection: 'row',
  },
  spacer: {
    margin: 15,
  },
  spacerSmall: {
    margin: 10,
  },
  title: {
    ...textStyles.xLargeBold,
    color: Palette.primary,
    width: width / 2,
  },
  subtitle: {
    ...textStyles.largeBold,
    color: 'grey',
  },
  overview: {
    ...textStyles.largeSemibold,
    color: 'grey',
  },
  contentMain: {
    marginTop: width / 2.5 / 2,
    paddingHorizontal: 15,
  },
  rateBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
