import React, { useEffect } from 'react';
import { StyleSheet, ActivityIndicator, FlatList, View } from 'react-native';
import { Palette } from '../../utils/palette';
import textStyles from '../../utils/typography';
import TVCategoryItem from './components/TVCategoryItem';

const ListCategoryTV = ({ route, navigation }: NavigationType) => {
  const { loading, data, title } = route?.params;
  useEffect(() => {
    navigation.setOptions({
      headerTitle: title,
      headerTintColor: Palette.primary,
      headerTitleStyle: {
        ...textStyles.xLargeSemibold,
      },
    });
  }, [data.name, navigation, route, title]);
  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.container}>
          <ActivityIndicator size={25} color={Palette.text} />
        </View>
      ) : (
        <FlatList<TvData>
          data={data?.tv}
          renderItem={({ item }) => (
            <TVCategoryItem item={item} navigation={navigation} />
          )}
          keyExtractor={(item, index) => 'key' + index}
        />
      )}
    </View>
  );
};

export default ListCategoryTV;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
