import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useAiringTodayTvShows from '../../../hooks/useAiringTodayTvShows';
import { Palette } from '../../../utils/palette';

const AiringTV = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useAiringTodayTvShows<TvType>();

  useEffect(() => {
    console.log(error);
  }, [data?.tv, error]);
  return (
    <>
      <ContentTitle
        title={'Airing Today'}
        onPress={() => {
          navigation.navigate('ListCategoryTV', {
            title: 'Airing Today TV',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<TvData>
          horizontal
          data={data?.tv}
          renderItem={({ item }) => (
            <PosterMovies isMovie={false} item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default AiringTV;
