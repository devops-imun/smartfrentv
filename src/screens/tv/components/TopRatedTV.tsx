import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useTopRatedTvShows from '../../../hooks/useTopRatedTvShows';
import { Palette } from '../../../utils/palette';

const TopRatedTv = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useTopRatedTvShows<TvType>();

  useEffect(() => {
    console.log(error);
  }, [data?.tv, error]);
  return (
    <>
      <ContentTitle
        title={'Top Rated'}
        onPress={() => {
          navigation.navigate('ListCategoryTV', {
            title: 'Top Rated TV',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<TvData>
          horizontal
          data={data?.tv}
          renderItem={({ item }) => (
            <PosterMovies isMovie={false} item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default TopRatedTv;
