import React from 'react';
import { Image, Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import { Palette } from '../../../utils/palette';
import { width } from '../../../utils/size';
import textStyles from '../../../utils/typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

const TVCategoryItem = ({ item, navigation }: any) => {
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('DetailMovie', {
          data: item,
        })
      }
      style={styles.card}
    >
      <Image
        source={{
          uri: item?.backdrop?.medium,
        }}
        style={styles.image}
      />
      <View style={styles.content}>
        <View style={styles.headerCard}>
          <Text numberOfLines={2} style={styles.name}>
            {item?.name}
          </Text>
          <View style={styles.row}>
            <Ionicons
              name={'notifications-outline'}
              color={Palette.primary}
              size={25}
            />
            <View style={styles.spacerSmall} />
            <Ionicons
              name={'share-social-outline'}
              color={Palette.primary}
              size={25}
            />
          </View>
        </View>
        <Text style={styles.overview}>{item?.overview}</Text>
        <Text style={styles.overview}>{item?.seasons?.name}</Text>
        <View style={styles.spacerSmall} />
        {/* <View style={styles.genderContainer}>
          {item?.genres.map((el: MovieGenres) => (
            <Text style={styles.genre}> {el.name} |</Text>
          ))}
        </View> */}
      </View>
    </TouchableOpacity>
  );
};

export default TVCategoryItem;

const styles = StyleSheet.create({
  card: {
    width: width,
    overflow: 'hidden',
    marginBottom: 15,
  },
  headerCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  row: {
    flexDirection: 'row',
  },
  spacer: {
    marginVertical: 10,
  },
  spacerSmall: {
    margin: 5,
  },
  image: {
    width: width,
    height: width / 1.5,
  },
  content: {
    padding: 10,
  },
  name: {
    ...textStyles.xLargeBold,
    color: Palette.primary,
  },
  overview: {
    ...textStyles.largeLight,
    color: Palette.shadow,
  },
  genderContainer: {
    flexDirection: 'row',
  },
  genre: {
    ...textStyles.largeBold,
    color: Palette.primary,
  },
});
