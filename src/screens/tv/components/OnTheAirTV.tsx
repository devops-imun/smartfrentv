import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import ContentTitle from '../../../components/common/ContentTitle';
import PosterMovies from '../../../components/common/PosterMovies';
import useOnTheAirTvShows from '../../../hooks/useOnTheAirTvShows';
import { Palette } from '../../../utils/palette';

const OnTheAirTV = ({ navigation }: NavigationType) => {
  const { loading, error, data } = useOnTheAirTvShows<TvType>();

  useEffect(() => {
    console.log(error);
  }, [data?.tv, error]);
  return (
    <>
      <ContentTitle
        title={'On the air'}
        onPress={() => {
          navigation.navigate('ListCategoryTV', {
            title: 'On The Air TV',
            loading: loading,
            data: data,
          });
        }}
      />
      {loading ? (
        <ActivityIndicator size={25} color={Palette.text} />
      ) : (
        <FlatList<TvData>
          horizontal
          data={data?.tv}
          renderItem={({ item }) => (
            <PosterMovies isMovie={false} item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

export default OnTheAirTV;
