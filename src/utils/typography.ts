import { StyleSheet } from 'react-native';

const textStyles = StyleSheet.create({
  smallLight: {
    fontSize: 10,
    fontWeight: '400',
  },
  smallMedium: {
    fontSize: 10,
    fontWeight: '600',
  },
  smallSemibold: {
    fontSize: 10,
    fontWeight: '800',
  },
  smallBold: {
    fontSize: 10,
    fontWeight: 'bold',
  },

  mediumLight: {
    fontSize: 12,
    fontWeight: '400',
  },
  mediumMedium: {
    fontSize: 12,
    fontWeight: '600',
  },
  mediumSemibold: {
    fontSize: 12,
    fontWeight: '800',
  },
  mediumBold: {
    fontSize: 12,
    fontWeight: 'bold',
  },

  largeLight: {
    fontSize: 14,
    fontWeight: '400',
  },
  largeMedium: {
    fontSize: 14,
    fontWeight: '600',
  },
  largeSemibold: {
    fontSize: 14,
    fontWeight: '800',
  },
  largeBold: {
    fontSize: 14,
    fontWeight: 'bold',
  },

  xLargeLight: {
    fontSize: 18,
    fontWeight: '400',
  },
  xLargeMedium: {
    fontSize: 18,
    fontWeight: '600',
  },
  xLargeSemibold: {
    fontSize: 18,
    fontWeight: '800',
  },
  xLargeBold: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default textStyles;
