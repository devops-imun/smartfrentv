export const Palette = {
  primary: 'white',
  card: '#444444',
  border: 'rgb(28, 28, 30)',
  text: 'rgb(255, 255, 255)',
  shadow: '#444444',
};
