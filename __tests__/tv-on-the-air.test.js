import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { Text } from 'react-native';
import { GET_ON_THE_AIR_TV_SHOWS } from '../src/hooks/useOnTheAirTvShows';

const mocks = [
  {
    request: {
      query: GET_ON_THE_AIR_TV_SHOWS,
    },
    result: {
      data: {
        tv: [
          {
            id: '1',
            name: 'name',
            overview: 'overview',
            popularity: 'popularity',
          },
        ],
      },
    },
  },
];

///test must be load data movie now playing
it('Get on the air tv', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
