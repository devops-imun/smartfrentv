import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { Text } from 'react-native';
import { GET_AIRING_TODAY } from '../src/hooks/useAiringTodayTvShows';

const mocks = [
  {
    request: {
      query: GET_AIRING_TODAY,
    },
    result: {
      data: {
        tv: [
          {
            id: '1',
            name: 'name',
            overview: 'overview',
            popularity: 'popularity',
          },
        ],
      },
    },
  },
];

///test must be load data movie now playing
it('Get airing today tv', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
