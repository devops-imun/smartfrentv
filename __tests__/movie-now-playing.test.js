import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { GET_NOW_PLAYING_MOVIE } from '../src/hooks/useNowPlayingMovies';
import { Text } from 'react-native';

const mocks = [
  {
    request: {
      query: GET_NOW_PLAYING_MOVIE,
    },
    result: {
      data: {
        movie: [{ id: '1', name: 'name', overview: 'overview' }],
      },
    },
  },
];

///test must be load data movie now playing
it('Get now playing movie', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
