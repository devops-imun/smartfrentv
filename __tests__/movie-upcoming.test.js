import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { Text } from 'react-native';
import { GET_UPCOMING_MOVIES } from '../src/hooks/useUpcomingMovies';

const mocks = [
  {
    request: {
      query: GET_UPCOMING_MOVIES,
    },
    result: {
      data: {
        movie: [{ id: '1', name: 'name', overview: 'overview' }],
      },
    },
  },
];

///test must be load data movie now playing
it('Get upcoming movie', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
