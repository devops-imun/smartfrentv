import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { Text } from 'react-native';
import { GET_POPULAR_TV_SHOWS } from '../src/hooks/usePopularTvShows';

const mocks = [
  {
    request: {
      query: GET_POPULAR_TV_SHOWS,
    },
    result: {
      data: {
        tv: [
          {
            id: '1',
            name: 'name',
            overview: 'overview',
            popularity: 'popularity',
          },
        ],
      },
    },
  },
];

///test must be load data movie now playing
it('Get get popular tv', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
