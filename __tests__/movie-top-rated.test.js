import React from 'react';
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';
import { Text } from 'react-native';
import { GET_TOP_RATED_MOVIE } from '../src/hooks/useTopRatedMovies';

const mocks = [
  {
    request: {
      query: GET_TOP_RATED_MOVIE,
    },
    result: {
      data: {
        movie: [{ id: '1', name: 'name', overview: 'overview' }],
      },
    },
  },
];

///test must be load data movie top rated
it('Get top rated movie', async () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Text>OK</Text>
    </MockedProvider>,
  );
  await 0;

  const tree = component.toJSON();
  expect(tree.children).toMatchSnapshot();
});
