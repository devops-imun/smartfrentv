/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useCallback, useEffect, useState } from 'react';
import type { Node } from 'react';
import { DevSettings, View, ActivityIndicator } from 'react-native';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
} from '@apollo/client';
import RouteNavigator from './src/screens/navigation/index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CachePersistor, AsyncStorageWrapper } from 'apollo3-cache-persist';
import {
  TopRatedMoviesContext,
  topRatedMoviesReducer,
} from './src/context/TopRatedMoviesContext';
import config from './src/config/config';
import { StyleSheet } from 'react-native';
import { Palette } from './src/utils/palette';

export const link = createHttpLink({
  uri: config.baseUrl,
});

const App = () => {
  const [client, setClient] = useState<ApolloClient<NormalizedCacheObject>>();
  const [persistor, setPersistor] =
    useState<CachePersistor<NormalizedCacheObject>>();

  useEffect(() => {
    async function init() {
      const cache = new InMemoryCache();
      let newPersistor = new CachePersistor({
        cache,
        storage: new AsyncStorageWrapper(AsyncStorage),
        // debug: __DEV__,
      });
      await newPersistor.restore();
      setPersistor(newPersistor);
      setClient(
        new ApolloClient({
          cache,
          link: link,
        }),
      );
    }

    init();
  }, []);

  // const clearCache = useCallback(() => {
  //   if (!persistor) {
  //     return;
  //   }
  //   persistor.purge();
  // }, [persistor]);

  // const reload = useCallback(() => {
  //   DevSettings.reload();
  // }, []);

  if (!client) {
    return (
      <View style={styles.container}>
        <ActivityIndicator color={Palette.primary} size={'large'} />
      </View>
    );
  }

  // const initialState = useContext(TopRatedMoviesContext);
  // const [state, dispatch] = useReducer(topRatedMoviesReducer, initialState);
  return (
    <ApolloProvider client={client}>
      {/* <TopRatedMoviesContext.Provider value={{ state, dispatch }}> */}
      <RouteNavigator />
      {/* </TopRatedMoviesContext.Provider> */}
    </ApolloProvider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
